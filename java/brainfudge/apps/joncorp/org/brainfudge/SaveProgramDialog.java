package brainfudge.apps.joncorp.org.brainfudge;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * A Dialog for saving programs written in the BF
 * Created by jon on 11/01/15.
 */
public class SaveProgramDialog extends DialogFragment
{
    public interface SaveProgramDialogResponse
    {
        void onSaveProgramDialogResponse(boolean _success,String _filename);
    }

    private InputMethodManager mKeyboard;
    private SaveProgramDialogResponse mResponse = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        //mResponse = null;

        mKeyboard = (InputMethodManager) this.getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.save_dialog, null))
                // Add action buttons
                .setPositiveButton(R.string.dialogSave, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText et = SaveProgramDialog.this.getDialog().findViewById(R.id.saveDialogFileName);

                        hideKeyboardOnExit(et);

                        if (mResponse != null)
                        {
                            mResponse.onSaveProgramDialogResponse(true, et.getText().toString());
                        }
                    }
                })
                .setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        EditText et = SaveProgramDialog.this.getDialog().findViewById(R.id.saveDialogFileName);

                        hideKeyboardOnExit(et);

                        SaveProgramDialog.this.getDialog().cancel();

                        if (mResponse != null)
                        {
                            mResponse.onSaveProgramDialogResponse(false, "");
                        }
                    }
                });
        return builder.create();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        AlertDialog ad = (AlertDialog)getDialog();
        Button b = ad.getButton(DialogInterface.BUTTON_POSITIVE);
        b.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.themeAction));
        b.setTextColor(ContextCompat.getColor(getContext(),R.color.themeFont));
        b = ad.getButton(DialogInterface.BUTTON_NEGATIVE);
        b.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.themeAction));
        b.setTextColor(ContextCompat.getColor(getContext(),R.color.themeFont));

    }

    public void RegisterResponseCallback(SaveProgramDialogResponse _callback)
    {
        mResponse = _callback;
    }

    private void hideKeyboardOnExit(View _v)
    {
        mKeyboard.hideSoftInputFromWindow(_v.getWindowToken(),0);
    }
}
