package brainfudge.apps.joncorp.org.brainfudge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

/**
 * Provides a dialog to enter a value to be passed to the BF interpreter
 * Created by jon on 12/01/15.
 */
public class InputDialog extends DialogFragment implements DialogInterface.OnClickListener,
                                                           View.OnClickListener
{
    private TextView mInputValue;
    private TextView mInputValueChar;
    private int mValue;

    public interface InputDialogResponse
    {
        void InputSelected(boolean _success,int _value);
    }

    private InputDialogResponse mResponse = null;

    public InputDialog()
    {
        mValue = 0;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View frag = inflater.inflate(R.layout.input_dialog,null);

        mInputValue = frag.findViewById(R.id.inputValue);
        mInputValueChar = frag.findViewById(R.id.inputValueChar);

        GridLayout gl = frag.findViewById(R.id.inputDialogKeyboardContainer);

        //find buttons
        for(int x=0;x<gl.getChildCount();x++)
        {
            if(gl.getChildAt(x) instanceof Button)
            {
                Button b = (Button)gl.getChildAt(x);
                b.setOnClickListener(this);
            }
        }

        builder.setView(frag);

        return builder.create();
    }

    //button press
    @Override
    public void onClick(View _v)
    {
        int newInput;
        boolean updateCharValue = false;
        if(_v.getId() == R.id.inputKeypadDel)
        {
            int l = mInputValue.getText().length();
            if(l > 1)
            {
                mInputValue.setText(mInputValue.getText().subSequence(0,l - 1));
            }
            else
            {
                mInputValue.setText("0");
            }

            try
            {
                newInput = Integer.parseInt(mInputValue.getText().toString());
                mValue = newInput;
            }
            finally
            {

            }

            updateCharValue = true;
        }
        else if(_v.getId() == R.id.inputKeypadOk)
        {
            getDialog().dismiss();
            if(mResponse != null)
            {
                mResponse.InputSelected(true,mValue);
            }
        }
        else
        {
            Button b = (Button)_v;
            StringBuilder i = new StringBuilder(mInputValue.getText());

            if(i.toString().equals("0"))
            {
                i.setLength(0);
                i.append(b.getText());
            }
            else
            {
                i.append(b.getText());
            }

            try
            {
                newInput = Integer.parseInt(i.toString());

                if(newInput < 256)
                {
                    mInputValue.setText(i);
                    mValue = newInput;
                }
            }
            finally
            {

            }

            updateCharValue = true;
        }

        if(updateCharValue)
        {
            if(mValue > 33)
            {
                mInputValueChar.setText(String.valueOf((char)mValue));
            }
            else if(mValue == 13)
            {
                mInputValueChar.setText("CR");
            }
            else
            {
                mInputValueChar.setText(" ");
            }
        }
    }

    @Override
    public void onCancel(DialogInterface dialogInterface)
    {
        mResponse.InputSelected(false,0);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i)
    {
        if(mResponse != null)
        {
            mResponse.InputSelected(true,mValue);
        }
    }

    public void RegisterResponseCallback(InputDialogResponse _callback)
    {
        mResponse = _callback;
    }
}
