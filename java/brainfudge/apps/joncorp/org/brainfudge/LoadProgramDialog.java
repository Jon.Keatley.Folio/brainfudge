package brainfudge.apps.joncorp.org.brainfudge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import java.io.File;

/**
 * Created by jon on 12/01/15.
 */
public class LoadProgramDialog extends DialogFragment implements AdapterView.OnItemClickListener
{
    private File[] mFileOptions;

    public interface LoadProgramDialogResponse
    {
        void onLoadProgramDialogResponse(boolean _success,File _program);
    }

    private LoadProgramDialogResponse mResponse = null;

    public LoadProgramDialog()
    {
        mFileOptions = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View frag = inflater.inflate(R.layout.load_dialog,null);

        //populate list
        if(mFileOptions != null && mFileOptions.length > 0) {

            ListView list = frag.findViewById(R.id.loadDialogList);

            String[] fileNames = new String[mFileOptions.length];

            for(int x=0;x<fileNames.length;x++)
            {
                fileNames[x] = mFileOptions[x].getName();
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    R.layout.bf_list_item, fileNames);

            list.setAdapter(adapter);

            list.setOnItemClickListener(this);
        }

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(frag)
                .setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        AlertDialog ad = (AlertDialog)getDialog();
        Button b = ad.getButton(DialogInterface.BUTTON_NEGATIVE);
        b.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.themeAction));
        b.setTextColor(ContextCompat.getColor(getContext(),R.color.themeFont));

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        if(mResponse != null)
        {
            mResponse.onLoadProgramDialogResponse(true,mFileOptions[i]);
        }
        getDialog().dismiss();
    }

    public void RegisterResponseCallback(LoadProgramDialogResponse _callback)
    {
        mResponse = _callback;
    }

    public void PassFileList(File[] _list)
    {
        mFileOptions = _list;
    }
}
