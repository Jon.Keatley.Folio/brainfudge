package brainfudge.apps.joncorp.org.brainfudge;

import android.app.Activity;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.ViewFlipper;
import java.io.File;
import brainfudge.apps.joncorp.org.tools.FileManager;
import brainfudge.apps.joncorp.org.brainfudge.interpreter.brainFudgeInterpreter;
import brainfudge.apps.joncorp.org.brainfudge.interpreter.brainFudgeAsyncTask;
import brainfudge.apps.joncorp.org.tools.Storage;

public class BrainFudgeIDE extends Activity implements brainFudgeInterpreter.BFIO_interface,
                                                       SaveProgramDialog.SaveProgramDialogResponse,
                                                       LoadProgramDialog.LoadProgramDialogResponse,
                                                       PopupMenu.OnMenuItemClickListener,
                                                       ExamplesDialog.ExampleDialogResponse,
                                                       InputDialog.InputDialogResponse
{
    private EditText mCodeEditor;
    private TextView mProcessStep;
    private TextView mOutputView;

    private Button mHelpButton;
    private FrameLayout mHelpBar;
    private TransitionDrawable mHelpBarColorSwitch;

    private brainFudgeAsyncTask mInterpreterThread;

    private ViewFlipper mModeFlipper;
    private LinearLayout mKeyboard;
    private LinearLayout mAsciiContainer;
    private View mTabSelector;
    private boolean mIsShowingEditor;
    private boolean mIsShowingPopup;

    private PopupMenu mExtrasMenu;

    private SaveProgramDialog mSaveDialog;
    private LoadProgramDialog mLoadDialog;
    private ExamplesDialog mExampleDialog;
    private InputDialog mInputDialog;
    private InstructionsDialog mHelpDialog;

    private FileManager mFileManager;
    private Storage mStorage;
    private Toaster mToaster;

    private boolean mDynamicSizesSet;

    private StringBuilder mOutputTextBuilder;

    private static final int ANIMATION_SPEED = 250;
    private static final int ANIMATION_SPEED_SLOW = 800;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brain_fudge_ide);

        mDynamicSizesSet = false;

        mCodeEditor = findViewById(R.id.codeView);
        mModeFlipper = findViewById(R.id.modeFlipper);
        mProcessStep = findViewById(R.id.processView);
        mTabSelector = findViewById(R.id.tabSelector);
        mKeyboard = findViewById(R.id.keyboardContainer);

        //alter keyboard gravity if 10" tablet
        if(getResources().getInteger(R.integer.keyboardGravity) == 1)
        {
            ((GridLayout.LayoutParams)mKeyboard.getLayoutParams()).setGravity(Gravity.CENTER_HORIZONTAL);
        }

        mAsciiContainer = findViewById(R.id.asciiTableContainer);

        mExtrasMenu = new PopupMenu(this,findViewById(R.id.actMore));
        MenuInflater mf = mExtrasMenu.getMenuInflater();
        mf.inflate(R.menu.extra_menu,mExtrasMenu.getMenu());
        mExtrasMenu.setOnMenuItemClickListener(this);

        mOutputView = findViewById(R.id.outputView);
        mOutputView.setHorizontallyScrolling(true);
        mOutputView.setMovementMethod(new ScrollingMovementMethod());

        mIsShowingEditor = true;
        mIsShowingPopup = false;

        mSaveDialog = new SaveProgramDialog();
        mSaveDialog.RegisterResponseCallback(this);

        mLoadDialog = new LoadProgramDialog();
        mLoadDialog.RegisterResponseCallback(this);

        mExampleDialog = new ExamplesDialog();
        mExampleDialog.RegisterResponseCallback(this);

        mInputDialog = new InputDialog();
        mInputDialog.RegisterResponseCallback(this);

        mHelpDialog = new InstructionsDialog();

        //make selector highlight selected tab
        View editTab = findViewById(R.id.tabEditor);
        mTabSelector.setX(editTab.getX());

        mFileManager = new FileManager();
        mFileManager.SelectFolder("BrainFudge");

        mStorage = new Storage(getPreferences(MODE_PRIVATE));

        mToaster = new Toaster(getApplicationContext(),getLayoutInflater());

        mHelpBar = findViewById(R.id.helpBar);
        mHelpBarColorSwitch = (TransitionDrawable)mHelpBar.getBackground();

        float size = getResources().getDimension(R.dimen.standardButton);
        mHelpButton = findViewById(R.id.popupHelp);
        mHelpButton.setBackground(new TriangleDrawable(ContextCompat.getColor(getBaseContext(),R.color.themeAction),
                                  size,size));

//--- setup interpreter thread ----------------------------------------------------------

        mInterpreterThread = new brainFudgeAsyncTask(this,
                getResources().getString(R.string.errMsgLoopOpen),
                getResources().getString(R.string.errMsgLoopClose),
                getResources().getString(R.string.errMsgNeverEndingLoop));

        //populate ascii table
        String[] asciiCodes = new String[96];
        String tmp;

        for(int x=32, p = 0;x <= 126;x++,p++)
        {
            tmp = String.valueOf(x);
            if(tmp.length() == 2)
                tmp = " " + tmp;

            asciiCodes[p] = tmp;

            if(x == 32)
            {
                asciiCodes[p] += "  ";
            }
            else
            {
                asciiCodes[p] += " " + (char) x;
            }

        }

        asciiCodes[95] = "13 CR";

        GridView asciiGrid = findViewById(R.id.helpAsciiGrid);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.ascii_grid_cell, asciiCodes);

        asciiGrid.setAdapter(adapter);

        mOutputTextBuilder = new StringBuilder();
    }

//--- Life cycle events -------------------------------------------------------------

    @Override
    public void onResume()
    {
        super.onResume();

        String prog = mStorage.GetString("program");
        mStorage.RemoveValue("program");

        if(prog != null)
        {
            mCodeEditor.setText(prog);
            mCodeEditor.setSelection(prog.length(),prog.length());
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(mCodeEditor.length() > 0)
        {
            mStorage.SaveString("program",mCodeEditor.getText().toString());
        }
    }

//--- GUI events --------------------------------------------------------------------

    //action button events
    public void actionRequest(View _v)
    {
        switch(_v.getId())
        {
            case R.id.actRun:
                mOutputView.setText("");
                mOutputView.setScrollX(0);
                mOutputView.setScrollY(0);

                if(mCodeEditor.getText().length() == 0)
                {
                    mToaster.MakeToast(getResources().getString(R.string.errMsgEmpty),true);
                    break;
                }

                if(mInterpreterThread.isStarted())
                {
                    if(!mInterpreterThread.hasFinished())
                    {
                        mInterpreterThread.cancel(true);
                    }

                    mInterpreterThread = new brainFudgeAsyncTask(this,
                            getResources().getString(R.string.errMsgLoopOpen),
                            getResources().getString(R.string.errMsgLoopClose),
                            getResources().getString(R.string.errMsgNeverEndingLoop));
                }

                if(mInterpreterThread.LoadString(mCodeEditor.getText().toString()))
                {
                    if(mIsShowingEditor)
                    {
                        switchMode();
                    }

                    mInterpreterThread.execute();
                }
                else
                {
                    //output error
                    mToaster.MakeToast(getResources().getString(R.string.errMsgSyntax),true);
                }

                break;
            case R.id.actLoad:

                mLoadDialog.PassFileList(mFileManager.ListFiles(FileManager.BRAIN_FUDGE_FILE_TYPE));
                mLoadDialog.show(getFragmentManager(),"loadDialog");

                break;
            case R.id.actSave:

                if(mCodeEditor.getText().length() == 0)
                {
                    mToaster.MakeToast(getResources().getString(R.string.errMsgSaveEmpty),true);
                }
                else
                {
                    mSaveDialog.show(getFragmentManager(), "saveDialog");
                }
                break;
            case R.id.actMore:
                    mExtrasMenu.show();
                break;
        }
    }

    //help slide-in event
    public void popupSelect(View view)
    {
        //resize ascii window
        if(!mDynamicSizesSet)
        {
            mAsciiContainer.getLayoutParams().width = (int)(mModeFlipper.getMeasuredWidth() / 2.5f);

            //setup help bar height
            mHelpBar.getLayoutParams().height = mModeFlipper.getMeasuredHeight();
            mHelpBar.requestLayout();

            mDynamicSizesSet = true;
        }

        if(mIsShowingPopup)
        {
            mAsciiContainer.setVisibility(View.GONE);
            mAsciiContainer.requestLayout();
            mHelpButton.animate()
                    .rotation(0)
                    .setDuration(ANIMATION_SPEED)
                    .start();

            mHelpBarColorSwitch.reverseTransition(ANIMATION_SPEED_SLOW);
        }
        else
        {
            mAsciiContainer.setVisibility(View.VISIBLE);
            mAsciiContainer.requestLayout();
            mHelpButton.animate()
                    .rotation(180)
                    .setDuration(ANIMATION_SPEED)
                    .start();

            mHelpBarColorSwitch.startTransition(ANIMATION_SPEED_SLOW);
        }

        mIsShowingPopup = !mIsShowingPopup;
    }

    //main tab press event
    public void tabSelect(View _v)
    {
        if(_v.getId() == R.id.tabEditor && mIsShowingEditor)
        {
            return;
        }

        if(_v.getId() == R.id.tabOutput && !mIsShowingEditor)
        {
            return;
        }

        switchMode();
    }

    //"keyboard" events
    public void processInput(View _v)
    {
        if(_v instanceof Button)
        {
            Button b = (Button)_v;

            if(b.getId() == R.id.btnSpace)
            {
                updateEditor(" ");
            }
            else if(b.getId() == R.id.btnDel)
            {
                if(mCodeEditor.getText().length() > 0)
                {
                    int cursor = mCodeEditor.getSelectionStart();

                    if(cursor == 1)
                    {
                        mCodeEditor.setSelection(0,0);
                        mCodeEditor.setText(mCodeEditor.getText().subSequence(cursor,mCodeEditor.length()).toString());
                    }
                    else if(cursor > 1)
                    {
                        cursor--;
                        mCodeEditor.setText(mCodeEditor.getText().delete(cursor,cursor + 1).toString());

                        mCodeEditor.setSelection(cursor,cursor);
                    }
                }
            }
            else
            {
                updateEditor(b.getText().toString());
            }
        }
    }

    //extra menu presses
    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.extraClear:

                if(!mIsShowingEditor)
                {
                    switchMode();
                }
                mCodeEditor.setText("");
                break;
            case R.id.extraExamples:
                mExampleDialog.show(getFragmentManager(), "exampleDialog");
                break;
            case R.id.extraHelp:
                mHelpDialog.show(getFragmentManager(), "helpDialog");
                break;
        }

        return true;
    }

//--- helper methods ----------------------------------------------------------------------

    //update text on editor based on cursor position
    private void updateEditor(String _update)
    {
        int cursor = mCodeEditor.getSelectionStart();

        if(mCodeEditor.getSelectionEnd() != cursor)
        {
            mCodeEditor.setSelection(cursor,cursor);
        }

        mCodeEditor.getText().insert(cursor,_update);

    }

    //switch view mode from edit to program output
    private void switchMode()
    {
        mIsShowingEditor = !mIsShowingEditor;

        if(mIsShowingEditor)
        {
            mTabSelector.animate()
                    .translationX(0)
                    .setDuration(ANIMATION_SPEED)
                    .setListener(null);

            mKeyboard.animate()
                    .translationY(0)
                    .setDuration(ANIMATION_SPEED);

            mModeFlipper.setOutAnimation(getApplicationContext(),R.anim.slide_out);
            mModeFlipper.setInAnimation(getApplicationContext(),R.anim.slide_in);
            mModeFlipper.showNext();
        }
        else
        {
            Button b = findViewById(R.id.tabOutput);

            mTabSelector.animate()
                    .translationX(b.getX())
                    .setDuration(ANIMATION_SPEED)
                    .setListener(null);

            mKeyboard.animate()
                    .translationY(mKeyboard.getHeight())
                    .setDuration(ANIMATION_SPEED);

            mModeFlipper.setOutAnimation(getApplicationContext(),R.anim.slide_back_out);
            mModeFlipper.setInAnimation(getApplicationContext(),R.anim.slide_back_in);
            mModeFlipper.showPrevious();
        }
    }

//--- call back methods -------------------------------------------------------------------

    @Override
    public void BrainFudgeOutput(String _output)
    {
        mOutputView.append(_output);
    }

    @Override
    public void BrainFudgeInput()
    {
        mInputDialog.show(getFragmentManager(), "inputDialog");
    }

    @Override
    public void CurrentInstructionIndexChanged(int _stepCount,int _newPos,brainFudgeInterpreter.BF_Command _cmd)
    {
        mOutputTextBuilder.setLength(0);
        mOutputTextBuilder.append(getResources().getString(R.string.stepsTxt));
        mOutputTextBuilder.append(_stepCount);

        mProcessStep.setText(mOutputTextBuilder.toString());
    }

    @Override
    public void OnError(String _error)
    {
        mToaster.MakeToast(getResources().getString(R.string.errMsgTxt) + _error,true);
    }

//---------------------------------

    @Override
    public void onSaveProgramDialogResponse(boolean _success, String _filename)
    {
        if(_success) {
            if (mFileManager.SaveString(_filename, mCodeEditor.getText().toString())) {
                mToaster.MakeToast(_filename + getResources().getString(R.string.saveSuccessTxt), false);
            } else {
                if (mFileManager.HasError()) {
                    mToaster.MakeToast(getResources().getString(R.string.errMsgSaveErr), true);
                } else {
                    mToaster.MakeToast(getResources().getString(R.string.errMsgSaveErr), true);
                }
            }
        }
    }

    @Override
    public void onLoadProgramDialogResponse(boolean _success, File _program)
    {
        if(_success)
        {
            if(!mIsShowingEditor)
            {
                switchMode();
            }

            String s = mFileManager.LoadString(_program.getName());

            if(s.length() > 0)
            {
                mCodeEditor.setText(s);
                mCodeEditor.setSelection(mCodeEditor.length(),mCodeEditor.length());
            }
            else
            {
                if(mFileManager.HasError())
                {
                    mToaster.MakeToast(mFileManager.GetError(),true);
                }
            }
        }
    }

    @Override
    public void onExampleSelected(boolean _success, String _program)
    {
        if(_success)
        {
            mCodeEditor.setText(_program);
            mCodeEditor.setSelection(mCodeEditor.length(), mCodeEditor.length());

            if(!mIsShowingEditor)
            {
                switchMode();
            }
        }
    }

    @Override
    public void InputSelected(boolean _success, int _value)
    {
        if(_success)
        {
            mInterpreterThread.receiveInput(_value);
        }
        else
        {
            mInterpreterThread.receiveInput(0);
        }
    }
}
