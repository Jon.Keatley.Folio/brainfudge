package brainfudge.apps.joncorp.org.brainfudge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

/**
 * Created by jon on 12/01/15.
 */
public class InstructionsDialog extends DialogFragment
{
    public InstructionsDialog()
    {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View frag = inflater.inflate(R.layout.instructions_dialog,null);



        builder.setView(frag)
                .setPositiveButton(R.string.dialogOk, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        return builder.create();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        AlertDialog ad = (AlertDialog)getDialog();
        Button b = ad.getButton(DialogInterface.BUTTON_POSITIVE);
        b.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.themeAction));
        b.setTextColor(ContextCompat.getColor(getContext(),R.color.themeFont));

    }
}
