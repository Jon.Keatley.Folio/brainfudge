package brainfudge.apps.joncorp.org.brainfudge.interpreter;

/**
 * Created by jon on 20/11/14.
 * holds information about a loop
 */
public class brainFudgeLoop
{
    private int mStart;
    private int mEnd;
    private int mId;
    private int mCount;

    public brainFudgeLoop(int _id,int _start)
    {
        mId = _id;
        mStart = _start;
        mEnd = -1;
        mCount = 0;
    }

    public int getStart()
    {
        return mStart;
    }

    public int getEnd()
    {
        return mEnd;
    }

    public boolean hasEnd()
    {
        return mEnd > -1;
    }

    public int getId()
    {
        return mId;
    }

    public void setStart(int _start)
    {
        mStart = _start;
    }

    public void setEnd(int _end)
    {
        mEnd = _end;
    }

    public void touched() { mCount ++; }

    public int getTouches() { return mCount; }

    public void resetTouches() { mCount = 0; }
}
