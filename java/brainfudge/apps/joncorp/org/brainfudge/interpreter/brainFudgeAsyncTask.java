package brainfudge.apps.joncorp.org.brainfudge.interpreter;

import android.os.AsyncTask;
import android.util.Log;

import javax.xml.transform.Result;

/**
 * Async task to loop through the interpreter and pass back information as needed.
 * Created by jon on 11/02/15.
 */
public class brainFudgeAsyncTask extends AsyncTask<Void,Void,Void> implements brainFudgeInterpreter.BFIO_interface
{
    private static final String TAG = "BF_ASYNC_INTERPRETER";

    private brainFudgeInterpreter mInterpreter;
    private brainFudgeInterpreter.BFIO_interface mCallback;

    private StringBuilder mOutput;
    private int mInput;

    private int mStepCount;

    private boolean mWasStarted;
    private boolean mHasFinished;

    private String mExecutionError;

    public brainFudgeAsyncTask(brainFudgeInterpreter.BFIO_interface _callback,
                               String _msgLoopOpenErr,
                               String _msgLoopCloseErr,
                               String _msgLoopNeverEndingErr)
    {
        mCallback = _callback;
        mInterpreter = new brainFudgeInterpreter(this,_msgLoopOpenErr,_msgLoopCloseErr,_msgLoopNeverEndingErr);
        mOutput = new StringBuilder();
        mInput = -1;
        mStepCount = 0;
        mWasStarted = false;
        mHasFinished = false;
        mExecutionError= "";
    }

    public boolean isStarted()
    {
        return mWasStarted;
    }

    public boolean hasFinished()
    {
        return mHasFinished;
    }

    public boolean LoadString(String _prog)
    {
        return mInterpreter.LoadString(_prog);
    }

    public void receiveInput(int _input)
    {
        mInput = _input;
    }

    @Override
    protected Void doInBackground(Void... voids)
    {
        boolean run;

        while(true)
        {
            if(isCancelled())
            {
                break;
            }

            run = mInterpreter.ExecuteStep();

            if(run)
            {
                continue;
            }

            if(mInterpreter.hasEnded())
            {
                break;
            }


            while(true)
            {
                //sleep somehow
                try
                {
                    Thread.sleep(300);

                    if(mInput > -1)
                    {
                        mInterpreter.receiveInput(mInput);
                        mInput = -1;
                        Thread.sleep(150); //prevent next step being processed before the input
                        //dialog has finished
                        break;
                    }
                }
                catch(Exception ex)
                {
                    //nothing to do
                }

                if(isCancelled())
                {
                    break;
                }
            }
        }

        return null;
    }

    @Override
    protected void onPreExecute()
    {
        //reset everything
        mInput = -1;
        mStepCount = 0;
        mOutput.setLength(0);
        mWasStarted = true;
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        if(!isCancelled())
        {
            if(mExecutionError.length() > 0)
            {
                mCallback.OnError(mExecutionError);
                mCallback.BrainFudgeOutput(mExecutionError);
            }
            else
            {
                Log.d(TAG, "Outputting result of " + mOutput.length() + " chars");
                mCallback.CurrentInstructionIndexChanged(mStepCount, 0, brainFudgeInterpreter.BF_Command.Ignore);
                mCallback.BrainFudgeOutput(mOutput.toString());
            }
            mHasFinished = true;


        }
        else
        {
            Log.d(TAG,"No output as cancelled!");
        }
    }

//----------------------------------------------------------------------

    @Override
    public void BrainFudgeOutput(String _output)
    {
        mOutput.append(_output);
    }

    @Override
    public void BrainFudgeInput()
    {
        mCallback.BrainFudgeInput();
    }

    @Override
    public void CurrentInstructionIndexChanged(int _stepCount, int _newPos, brainFudgeInterpreter.BF_Command _cmd)
    {
        mStepCount = _stepCount;
    }

    @Override
    public void OnError(String _error)
    {
        mExecutionError = _error;
    }
}
