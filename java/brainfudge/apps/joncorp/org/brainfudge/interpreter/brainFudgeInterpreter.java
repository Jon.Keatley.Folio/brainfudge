package brainfudge.apps.joncorp.org.brainfudge.interpreter;

import android.util.Log;
import java.util.ArrayList;

/**
 * Created by jon on 16/11/14.
 * Accepts chars, converts to command enum and updates brain fudge state based on enum
 */
public class brainFudgeInterpreter
{
    public interface BFIO_interface
    {
        void BrainFudgeOutput(String _output);
        void BrainFudgeInput();
        void CurrentInstructionIndexChanged(int _stepCount,int _newPos,BF_Command _cmd);
        void OnError(String _error);
    }

    public enum BF_Command
    {
        NextPointer,
        PreviousPointer,
        PlusOne,
        MinusOne,
        StartLoop,
        EndLoop,
        Output,
        Input,
        Ignore,
        Illegal
    }

    private final static int BANKS_OF_STATE = 10; //do not change this number without looking at
                                                  //looking at clear method
    private final static int NUMBER_OF_BANKS = 30;
    private final static int STATE_SIZE = NUMBER_OF_BANKS * BANKS_OF_STATE;

    private final static int MAX_LOOP_ITERATION = 90000;

    private int mStatePointer;
    private int[] mState;

    private int mInstructionPointer;
    private ArrayList<BF_Command> mInstructions;
    private ArrayList<brainFudgeLoop> mLoopInfo;
    private int mNextLoopId;

    private boolean mReportSteps;

    private int mStepCount;

    private boolean mHasEnded;

    private BFIO_interface mIo;

    private static final String TAG = "BF_I";

    private String mErrorMsgLoopOpen;
    private String mErrorMsgLoopClose;
    private String mErrorMsgNeverEndingLoop;

    public brainFudgeInterpreter(BFIO_interface _interface,String _errorOpen, String _errorClose, String _errorNELoop)
    {
        mIo = _interface;
        mState = new int[STATE_SIZE];
        mInstructions = new ArrayList<>();
        mLoopInfo = new ArrayList<>();
        mReportSteps = true;
        mErrorMsgLoopOpen = _errorOpen;
        mErrorMsgLoopClose = _errorClose;
        mErrorMsgNeverEndingLoop = _errorNELoop;
        reset();
    }

    public void reset()
    {
        clearState();
        mInstructionPointer = 0;
        mStatePointer = 0;
        mNextLoopId = 0;
        mStepCount = 0;
        mInstructions.clear();
        mLoopInfo.clear();
        mHasEnded = false;
    }

    public boolean LoadCommandArray(BF_Command[] _cmds)
    {
        reset();
        int loopLevel = 0;

        for(int x=0;x < _cmds.length;x++)
        {
            mInstructions.add(_cmds[x]);
            if(_cmds[x] == BF_Command.StartLoop)
            {
                mLoopInfo.add(new brainFudgeLoop(mNextLoopId,x));

                mNextLoopId += 1;
                loopLevel += 1;
            }
            else if(_cmds[x] == BF_Command.EndLoop)
            {

                for(int l=mLoopInfo.size() - 1; l >= 0; l--)
                {
                    brainFudgeLoop bfl = mLoopInfo.get(l);
                    if(!bfl.hasEnd())
                    {
                        bfl.setEnd(x);
                        break;
                    }
                }

                loopLevel -= 1;
            }
        }

        if(loopLevel > 0)
        {
            //unclosed loop found
            reset();
        }

        return loopLevel == 0;
    }

    public boolean LoadString(String _strCmd)
    {
        reset();

        ArrayList<BF_Command> _cmds = new ArrayList<>();

        for(char c : _strCmd.toCharArray())
        {
            switch(c)
            {
                case '+':
                    _cmds.add(BF_Command.PlusOne);
                    break;
                case '-':
                    _cmds.add(BF_Command.MinusOne);
                    break;
                case '<':
                    _cmds.add(BF_Command.PreviousPointer);
                    break;
                case '>':
                    _cmds.add(BF_Command.NextPointer);
                    break;
                case '[':
                    _cmds.add(BF_Command.StartLoop);
                    break;
                case ']':
                    _cmds.add(BF_Command.EndLoop);
                    break;
                case '.':
                    _cmds.add(BF_Command.Output);
                    break;
                case ',':
                    _cmds.add(BF_Command.Input);
                    break;
                default:
                    break;
            }
        }

        return LoadCommandArray(_cmds.toArray(new BF_Command[_cmds.size()]));
    }

    public boolean hasEnded()
    {
        return mHasEnded;
    }

// command methods ------------------------------------------

    public boolean ExecuteStep()
    {
        if(mInstructions.size() == 0)
        {
            return false;
        }

        if(mInstructionPointer < mInstructions.size())
        {
            return ExecuteCommand(mInstructions.get(mInstructionPointer));
        }
        else
        {
            mHasEnded = true;
            return false;
        }
    }

    private boolean ExecuteCommand(BF_Command _cmd)
    {
        brainFudgeLoop bfl;

        boolean continueFlowing = true;
        mStepCount++;
        switch(_cmd)
        {
            case NextPointer:
                movePointer(true);
                break;
            case PreviousPointer:
                movePointer(false);
                break;
            case PlusOne:
                plusOne();
                break;
            case MinusOne:
                minusOne();
                break;
            case StartLoop:

                bfl = findLoop(mInstructionPointer,true);

                if(bfl == null)
                {
                    mIo.OnError(mErrorMsgLoopOpen);
                    mHasEnded = true;
                    return false;
                }

                if(getCurrentStateValue() == 0)
                {
                    //skip loop
                    bfl.resetTouches();
                    mInstructionPointer = bfl.getEnd();
                }
                break;
            case EndLoop:
                if(getCurrentStateValue() > 0)
                {
                    bfl = findLoop(mInstructionPointer,false);
                    if(bfl == null)
                    {
                        mIo.OnError(mErrorMsgLoopClose);
                        mHasEnded = true;
                        return false;
                    }

                    bfl.touched();

                    if(bfl.getTouches() >= MAX_LOOP_ITERATION)
                    {
                        Log.d(TAG,"Stopping loop");
                        mIo.OnError(mErrorMsgNeverEndingLoop);
                        mHasEnded = true;
                        return false;
                    }

                    mInstructionPointer = bfl.getStart();
                }
                break;
            case Output:
                outputState();
                break;
            case Input:
                requestInput();
                continueFlowing = false;
                break;
        }

        if(continueFlowing)
        {
            nextInstruction();
        }

        return continueFlowing;
    }

    private void outputState()
    {
        if(mState[mStatePointer] == 13)
        {
            mIo.BrainFudgeOutput("\n");
        }
        else
        {
            String out = String.valueOf((char)mState[mStatePointer]);
            mIo.BrainFudgeOutput(out);
        }
    }

    private void requestInput()
    {
        mIo.BrainFudgeInput();
    }

    public void receiveInput(int _input)
    {
        mState[mStatePointer] = _input;
        nextInstruction();
    }

    private void nextInstruction()
    {
        mInstructionPointer += 1;

        if(mInstructionPointer < mInstructions.size() && mReportSteps) {
            mIo.CurrentInstructionIndexChanged(mStepCount,mInstructionPointer, mInstructions.get(mInstructionPointer));
        }
    }

    private int getCurrentStateValue()
    {
        return mState[mStatePointer];
    }

    private void plusOne()
    {
        if(mState[mStatePointer] + 1 < 255)
        {
            mState[mStatePointer] += 1;
        }
        else
        {
            mState[mStatePointer] = 0;
        }
    }

    private void minusOne()
    {
        if(mState[mStatePointer] > 0)
        {
            mState[mStatePointer] -=1;
        }
    }

    private void movePointer(boolean isLeft)
    {
        if(isLeft)
        {
            if(mStatePointer + 1 < mState.length)
            {
                mStatePointer += 1;
            }
            else
            {
                mStatePointer = 0;
            }
        }
        else
        {
            if(mStatePointer - 1 >= 0)
            {
                mStatePointer -= 1;
            }
            else
            {
                mStatePointer = mState.length -1;
            }
        }
    }

// helper methods ------------------------------------------------------

    private brainFudgeLoop findLoop(int _pos,boolean _isStart)
    {
        for(brainFudgeLoop bfl:mLoopInfo)
        {
            if(_isStart)
            {
                if(bfl.getStart() == _pos)
                {
                    return bfl;
                }
            }
            else
            {
                if(bfl.getEnd() == _pos)
                {
                    return bfl;
                }
            }
        }

        return null;
    }

    private void clearState()
    {
        for(int x=0;x<NUMBER_OF_BANKS;x++)
        {
            mState[x] = 0;
            mState[x +1] = 0;
            mState[x +2] = 0;
            mState[x +3] = 0;
            mState[x +4] = 0;
            mState[x +5] = 0;
            mState[x +6] = 0;
            mState[x +7] = 0;
            mState[x +8] = 0;
            mState[x +9] = 0;
        }
    }

}
