package brainfudge.apps.joncorp.org.brainfudge;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by jon on 12/01/15.
 */
public class ExamplesDialog extends DialogFragment implements AdapterView.OnItemClickListener
{
    private String[] mExampleNames;
    private String[] mExamples;

    public interface ExampleDialogResponse
    {
        void onExampleSelected(boolean _success,String _program);
    }

    private ExampleDialogResponse mResponse = null;

    public ExamplesDialog()
    {
        mExampleNames = new String[5];
        mExamples = new String[5];

        mExampleNames[0] = "Hello World";
        mExamples[0] = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";

        mExampleNames[1] = "ABC";
        mExamples[1] = "++++ [>>>+++++ +++++<<<-] >>> +++ +++ <<< +++ +++ [>+++++ +++++<-] > +++++ > +++++ [<<+++++>>-] <<+ [>.+>>.<<<-]";

        mExampleNames[2] = "Square";
        mExamples[2] = ">>>> +++++ +++++ +++ <<<< +++++ +++++ [>+++>++++<<-] > ++ > ++ ...... >> . << . < .... > . >> . << . < .... > . >> . << . < .... > .  >> . << ......";

        mExampleNames[3] = "JonCorp Logo";
        mExamples[3] = "> +++++ +++++ +++ < +++++ +++++ [>> +++ > +++ > +++ > +++ <<<<< -] >> ++ >>> +++++ <<< << +++++ [>>> ++ > ++ <<<<-] >>>> +++ < +++ +++ <" +
                ">>>......<<<.>>>......<<<.>>>......<<<.............>>>......<<<........<.>..>>>..<<<...>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<..........>>>...<<<......>>>...<<<.....<.>..>>>..<<<...>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<..>.<.>.<.>.<.>.<.>>>.<<<...>>>.<<<..>>>.<<<...>>>.<<<..>.<.>.<<.>..>>>..<<<...>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<.>.<.>.<.>.<.>.<..>>>.<<<.>>>.<<<.>>>.<<<..>>>.<<<.>>>.<<<.>>>.<<<.>.<.>.<.<.>..>>>..<<<...>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<..>.<.>.<.>.<.>.<.>>>...<<<.>>>.<<<..>>>.<<<.>>>...<<<..>.<.>.<<.>>>>....<<<...>>>......<<<.>>>..<<<..>>>..<<<.>.<.>.<.>.<.>.<....>>>.<<<......>>>.<<<...>.<.>.<.<.>>>>....<<<...>>>......<<<.>>>..<<<..>>>..<<<..>.<.>.<.>.<.>.<.>.<.>>>.<<<.>>....<<..>>>.<<<...>.<.>.<<.>.............................>.<..>>>.<<<.>>.<..>.<<...>>>.<<<.>.<...<.>>>>......<<<.>>>......<<<.>>>......<<<.>>>......<<<.>.<.>.<.>>>.<<<.>>.<..>.<<...>>>.<<<...>>>.<<<.<.>>>>..<<<.....>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<..>.<..>>>.<<<..>>..<<.....>>>.<<<.>>>.<<<.>>>.<<<<.>>>>..<<<.....>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<.>>>..<<<..>>>..<<<.>.<.>.<.>>>.<<<.>>>.<<<...>>>.<<<...>>>.<<<.>>>.<<<.>>>.<<<<.>>>>..<<<.....>>>..<<<..>>>..<<<.>>>......<<<.>>>......<<<..>.<..>>>.<<<..>>>...<<<.....>>>.<<<..>>>.<<<<.>>>>..<<<.....>>>..<<<..>>>..<<<.>>>.....<<<..>>>......<<<.>.<.>.<.>>>.<<<............>>>.<<<.<.>>>>......<<<.>>>......<<<.>>>..<<<.>>>...<<<.>>>..<<<.........>>>.<<<.>>>.....<<<.>>>...<<<.>>>.<<<..<.>>>>......<<<.>>>......<<<.>>>..<<<..>>>..<<<.>>>..<<<.........>>>...<<<...>>>...<<<.>>>...<<<..<.>";

        mExampleNames[4] = "X + Y = N";
        mExamples[4] = ",>,<[>+<-]>.";
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View frag = inflater.inflate(R.layout.examples_dialog,null);


        ListView list = frag.findViewById(R.id.examplesDialogList);


        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                R.layout.bf_list_item, mExampleNames);

        list.setAdapter(adapter);

        list.setOnItemClickListener(this);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(frag)
                .setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        return builder.create();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        AlertDialog ad = (AlertDialog)getDialog();
        Button b = ad.getButton(DialogInterface.BUTTON_NEGATIVE);
        b.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.themeAction));
        b.setTextColor(ContextCompat.getColor(getContext(),R.color.themeFont));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
    {
        if(mResponse != null)
        {
            mResponse.onExampleSelected(true,mExamples[i]);
        }
        getDialog().dismiss();
    }

    public void RegisterResponseCallback(ExampleDialogResponse _callback)
    {
        mResponse = _callback;
    }
}
