package brainfudge.apps.joncorp.org.brainfudge;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.Toast;

/**
 * abstract toast logic so that i can be extended in the future
 * Created by jon on 12/01/15.
 */
public class Toaster
{
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private Toast mToast;


    public Toaster(Context _context,LayoutInflater _inflater)
    {
        mContext = _context;
        mLayoutInflater = _inflater;
    }

    public void MakeToast(String _text,boolean _isLong)
    {
        mToast = Toast.makeText(mContext,_text,(_isLong?Toast.LENGTH_LONG:Toast.LENGTH_SHORT));
        mToast.show();
    }
}
