package brainfudge.apps.joncorp.org.brainfudge;

import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;


/**
 * Drawable that draws a triangle because it doesn't exit in xml form
 * Created by jon on 18/01/15.
 */
public class TriangleDrawable extends ShapeDrawable
{
    PathShape mPathShape;

    public TriangleDrawable(int _color,float _width,float _height)
    {
        super();

        float halfHeight = _height / 2;
        float twoThirdsWidth = _width - (_width / 3);

        Path path = new Path();
        path.moveTo(0, halfHeight);
        path.lineTo(_width,0);
        path.lineTo(twoThirdsWidth,halfHeight);
        path.lineTo(_width,_height);
        path.lineTo(0,halfHeight);
        path.close();

        mPathShape = new PathShape(path,_width,_height);

        setShape(mPathShape);
        getPaint().setColor(_color);
    }

    @Override
    protected void onBoundsChange(Rect _rect)
    {
        super.onBoundsChange(_rect);

        mPathShape.resize(_rect.width(),_rect.height());
    }

}
