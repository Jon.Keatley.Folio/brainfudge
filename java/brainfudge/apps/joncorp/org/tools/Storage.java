package brainfudge.apps.joncorp.org.tools;

import android.content.SharedPreferences;

/**
 * Wrapper for shared preferences
 * Created by jon on 13/01/15.
 */
public class Storage
{

    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    public Storage(SharedPreferences _prefs)
    {
        mPreferences = _prefs;
        mEditor = mPreferences.edit();
    }

    public String GetString(String _name)
    {
        return mPreferences.getString(_name,null);
    }

    public int GetInt(String _name)
    {
        return mPreferences.getInt(_name,-1);
    }

    public void RemoveValue(String _name)
    {
        mEditor.remove(_name);
        mEditor.apply();
    }

    public void SaveString(String _name,String _value)
    {
        mEditor.putString(_name,_value);
        mEditor.apply();
    }

    public void SaveInt(String _name,int _value)
    {
        mEditor.putInt(_name,_value);
        mEditor.apply();
    }
}
