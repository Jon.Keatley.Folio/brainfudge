package brainfudge.apps.joncorp.org.tools;

import android.os.Environment;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Manages the loading and saving of files to the external storage
 * Created by jon on 11/01/15.
 */
public class FileManager
{
    private String mError;
    private String mCurrentFolder;

    public final static String BRAIN_FUDGE_FILE_TYPE = ".jbf";

    public FileManager()
    {
        Reset();
    }

    public void Reset()
    {
        mError = "";
    }

    public String GetError()
    {
        return mError;
    }

    public boolean HasError()
    {
        return mError.length() > 0;
    }

    public boolean SelectFolder(String _folder)
    {
        mCurrentFolder = _folder;
        return true;
    }

    public String getCurrentFolder()
    {
        return mCurrentFolder;
    }

    public boolean SaveString(String _name,String _contents)
    {
        Reset();
        if(CanWrite())
        {
            StringBuilder filePath = new StringBuilder();

            File path = getStoragePath();

            if(path == null)
            {
                mError = "Unable to create path!";
                return false;
            }

            filePath.append(_name);
            filePath.append(BRAIN_FUDGE_FILE_TYPE);

            File file = new File(path.getPath(),filePath.toString());

            if(file.exists())
            {
                mError = "File already exists!";
                return false;
            }
            else
            {
                try
                {
                    if(file.createNewFile())
                    {
                        FileWriter fw = new FileWriter(file);

                        fw.write(_contents);

                        fw.close();

                        return true;
                    }
                    else
                    {
                        mError = "Failed to create file!";
                        return false;
                    }
                }
                catch(Exception ex)
                {
                    mError = "Failed to create file! Exception " + ex.toString();
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    public File[] ListFiles(final String _fileType)
    {
        Reset();

        if(HasStorage())
        {
            File root = getStoragePath();
            return root.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file)
                {
                    return file.getPath().endsWith(_fileType);
                }
            });
        }

        return new File[0];
    }

    public String LoadString(String _name)
    {
        Reset();

        String result = "";

        if(HasStorage())
        {
            File path = getStoragePath();

            if(path == null)
            {
                mError = "Unable to create path!";
                return result;
            }


            File file = new File(path.getPath(),_name);

            if(file.exists())
            {
                StringBuilder loader = new StringBuilder();
                try
                {
                    FileReader fr = new FileReader(file);

                    int b = 0;
                    while((b = fr.read()) != -1)
                    {
                        loader.append((char)b);
                    }

                    result = loader.toString();

                    fr.close();
                }
                catch (Exception ex)
                {
                    result = "";
                    mError = "Failed to load file!";
                }

            }
            else
            {
                mError = "File does not exist!";
            }
        }

        return result;
    }

    public boolean HasStorage()
    {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean CanWrite()
    {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

//--- helper methods ------------------------------------------

    private File getStoragePath()
    {
        if(mCurrentFolder.length() > 0)
        {
            return selectOrCreateDirectory(mCurrentFolder);
        }
        else
        {
            return getStorageRootDirectory();
        }
    }

    private File getStorageRootDirectory()
    {
        return Environment.getExternalStorageDirectory();
    }

    private File selectOrCreateDirectory(String _folder)
    {
        File f = getStorageRootDirectory();
        File newFile = new File(f.getPath(), _folder);

        if(!newFile.exists())
        {
            if(!newFile.mkdir())
            {
                newFile = null;
            }
        }

        return newFile;
    }
}
